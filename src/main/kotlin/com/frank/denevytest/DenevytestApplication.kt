package com.frank.denevytest


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication



@SpringBootApplication
class DenevytestApplication

fun main(args: Array<String>) {
	runApplication<DenevytestApplication>(*args)
}

