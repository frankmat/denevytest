package com.frank.denevytest

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository

interface BlogPostRepository : PagingAndSortingRepository<BlogPost, Long>, JpaSpecificationExecutor<BlogPost>










