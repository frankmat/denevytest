package com.frank.denevytest

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@javax.persistence.Entity
@javax.persistence.Table(name = "posts")
class BlogPost(
    @field:NotBlank(message = "Title is mandatory") val title: String?,
    @field:NotNull(message = "AuthorID is mandatory") val authorID: Long?,
    @field:NotBlank(message = "Content is mandatory") val contents: String?,
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val ID: Long?
) {

    override fun toString(): String {
        return "BlogPost(title=$title, authorID=$authorID, contents=$contents, ID=$ID)"
    }

}





