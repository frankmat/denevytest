package com.frank.denevytest

import au.com.console.jpaspecificationdsl.and
import au.com.console.jpaspecificationdsl.equal
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import javax.inject.Inject

@Service
class BlogPostService @Inject constructor(val blogPostRepository: BlogPostRepository) {

    fun findBlogPosts(
        blogPost: BlogPost,
        pageable: Pageable
    ): Page<BlogPost> = blogPostRepository
        .findAll(
            blogPost.toSpecification(),
            pageable
        )

    fun save(post: BlogPost):BlogPost = blogPostRepository.save(post)

    private fun hasAuthor(authorID: Long?): Specification<BlogPost>? = authorID?.let {
        BlogPost::authorID.equal(authorID)
    }

    private fun hasTitle(title:String?): Specification<BlogPost>? = title?.let {
        BlogPost::title.equal(title)
    }

    private fun hasID(ID: Long?): Specification<BlogPost>? = ID?.let {
        BlogPost::ID.equal(ID)
    }

    fun BlogPost.toSpecification():Specification<BlogPost> =
        and(
            hasID(this.ID),
            hasTitle(this.title),
            hasAuthor(this.authorID)
        )

}