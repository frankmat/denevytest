package com.frank.denevytest

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class Controller(
    private val blogPostService: BlogPostService
) {

    @PostMapping("/post")
    fun postBlogPost(@Valid @RequestBody post: BlogPost): ResponseEntity<String> {
        val blogPost = blogPostService.save(post)
        return ResponseEntity<String>("Blog post saved:\n $blogPost", HttpStatus.OK)
    }

    @GetMapping("/posts")
    fun getPosts(
        @RequestParam ID: Long?,
        @RequestParam title: String?,
        @RequestParam authorID: Long?,
        pageable: Pageable
    ): Page<BlogPost> = blogPostService.findBlogPosts(BlogPost(title, authorID, null, ID), pageable)

}







